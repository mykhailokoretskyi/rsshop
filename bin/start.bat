
SETLOCAL
SET VUID=999
SET DB_PORT=3306
SET WEB_ADMIN_PORT=80
SET PHP_DIR="%RSSHOP_HOME%/target/php"
SET DB_ROOT="%RSSHOP_HOME%/target/mysql"
SET DB_USER=vadik
SET DB_PASS=vadik
SET DB=vadik

IF EXIST "%PHP_DIR%" (
  mkdir "%PHP_DIR%"
  copy "%RSSHOP_HOME%/src/*" "%PHP_DIR%"
)

docker run -d ^
    --name mysql ^
    -p 3306:3306 ^
    -v "%DB_ROOT%":/var/lib/mysql ^
    -e MYSQL_USER="%DB_USER%" ^
    -e MYSQL_PASSWORD="%DB_PASS%" ^
    -e MYSQL_ROOT_PASSWORD="%DB_PASS%" ^
    -e MYSQL_DATABASE="%DB%" ^
    mysql:5.7

docker run ^
    -v "%PHP_DIR%":/data/www ^
    -v "%RSSHOP_HOME%"/conf/nginx:/data/conf/nginx/hosts.d ^
    -p 80:80 ^
    -p 9000:9000 ^
    --link mysql ^
    -it million12/nginx-php bash

ENDLOCAL
